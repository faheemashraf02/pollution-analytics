# **Pollution Analytics**

### Descriptive and Detailed study of air pollution in the IIT Delhi Campus.

Air pollution in India particularly in Delhi is increasing at a very alarming rate. The quality of air in Delhi has scared the public and has been a topic 
of discussion for pollution experts and public from past years. Moreover heath issues due to air pollution are also increasing at an alarming rate. In such 
situations proper air pollution monitoring is the need of hour as this can to a large extent help in controlling the situation. 

### METHODOLOGY

#### A. Study area

Delhi is a large metropolitan city having an area of 1484 Km2 and is bordered mostly by Haryana and Uttar Pradesh. In order to obtain an overview of air quality in Delhi region, air quality of three locations near IIT Delhi was monitored at different times in 24 hours for 7 days. The data was collected by using a low cost IOT device. This data was obtained from IIT, Delhi. It included air quality monitoring by observing trends in levels of PM 1, PM 2.5, PM 10. Data was collected from 15th to 21st October 2018 i.e. Monday to Sunday (1 week) at eight different times in 24 hours. The three prime locations for collecting the Particulate Matter data were:
Location 1 - IIT- Delhi main building.
Location 2 - Masala Mix near parking lot of IIT, Delhi.
Location 3 - Synergy building, TBIU lobby.

#### B. Attributes

The data obtained was plotted on a time series to obtain a clear image of PM level trends. The time series analysis was done on the basis of various attributes listed below:
Type of Particulate Matter
Location
Day
Time

#### C.  Exploratory data analysis

Exploratory data analysis (EDA) is an approach of analyzing data. This approach lets us figure out the main characteristics and summary along with graphical representations of our data. The descriptive methods of analysis in studying the parameters of mean,  median, standard deviation, mode, quartile, maximum value, minimum value and inter quartile range. The visual methods comprises of histograms, box plots, line plots, bar plots, scatter plots etc. have been implemented on  data to draw better insights from our hypothesis tests.

#### D. Data Wrangling

The data collected from the database is in raw format. This data needs to be transformed into a more valuable and usable format so that the process of analytics could be performed more accurately. The data points having high PM 2.5 concentrations (>1000 μg/m 3) are considered to be problematic outliers, so these data points were removed from the analysis. Following are the practices involved in the data wrangling:
a.Ignoring data sets where large amount of data is missing.
b.Removing or replacing outliers with mean/median/mode depending on the data set.
c.Changing data types into relevant data types.
d.Ignoring missing values.
e.Making time series values continuous.